import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);

const Store=new Vuex.Store({
    state:{
        //存储token
        userToken:localStorage.getItem('mytoken')?
        localStorage.getItem('mytoken'):'',
        baseurl:"http://localhost:5000/"
    },
    mutations:{
        //修改token，并将token存入localStorage
        changaLogin(state,username){
            state.userToken=username;
            localStorage.setItem('mytoken',username);
        },
        //退出登录
        exitLogin(state){
            state.userToken=null;
            localStorage.removeItem('mytoken');
        }
    },
    getters:{
        baseurl: state=> state.baseurl,
        userToken:state=>state.userToken
    }
});


export default Store