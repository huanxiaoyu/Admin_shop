import Vue from 'vue'
import App from './App.vue'
import router from './router'
import './plugins/element.js'
import './assets/css/global.css'
//import axios from 'axios'
import {fetch,post,postupload} from './plugins/httpUtils.js'
import store from './Vuex/store' //引用store.js
import './assets/font-awesome/css/font-awesome.min.css'
import base from './assets/js/base.js'


Vue.prototype.$get=fetch;//get请求
Vue.prototype.$post=post;//post请求
Vue.prototype.$postupload=postupload;//上传文件post请求

Vue.config.productionTip = false

Vue.use(base)

Vue.filter('dataFormat',function(originVal){
    const dt=new Date(originVal)

    const y=dt.getFullYear()
    const m=(dt.getMonth()+1+'').padStart(2,'0')
    const d=(dt.getDay()+'').padStart(2,'0')

    const hh=(dt.getHours()+'').padStart(2,'0')
    const mm=(dt.getMinutes()+'').padStart(2,'0')
    const ss=(dt.getSeconds()+'').padStart(2,'0')
    return y+'-'+m+'-'+d+' '+hh+':'+mm+':'+ss
})

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
