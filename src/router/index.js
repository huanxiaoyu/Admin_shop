import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Welcome from '../components/Welcome.vue'

Vue.use(VueRouter)

  const routes = [
  {
    path: '/home',
    name: 'Home',
    component: Home
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
  {
    path:'/login',
    name:'Login',
    component: ()=> import('../views/Login.vue')
  },
  {
    path:'/',
    name:'Index',
    component:()=>import('../views/Index.vue'),
    redirect:'/welcome',//访问/index就定义到/welcome
    children:[
      {
        path:'/welcome',
        component:Welcome
      },{
        path:'/user',
        component:()=>import('../views/Users/User.vue')
      },
      {
        path:'/rights',
        component:()=>import('../views/power/Rights.vue')
      },{
        path:"/role",
        component:()=>import('../views/power/Roles.vue')
      },{
        path:'/cate',
        component:()=>import('../views/Goods/Cate.vue')
      },{
        path:'/params',
        component:()=>import('../views/Goods/Params.vue')
      },{
        path:'/goodslist',
        component:()=>import('../views/Goods/list/GoodsList.vue')
      },{
        path:'/goodslist/addgoodslist',
        component:()=>import('../views/Goods/list/AddGoodsList.vue')
      },{
        path:'/goodslist/editgoodslist',
        component:()=>import('../views/Goods/list/EditGoodsList.vue')
      },{
        path:'/order',
        component:()=>import('../views/Order/Order.vue')
      },{
        path:'/report',
        component:()=>import('../views/Report/Report.vue')
      },{
        path:'/own',
        component:()=>import('../views/Own/Own.vue')
      },{
        path:'/password',
        component:()=>import('../views/Own/Password.vue')
      }

    ]
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

//导航守卫
//使用router.beforeEach注册一个全局前置守卫，判断用户是否登录
router.beforeEach((to,from,next)=>{
  if(to.path=='/login'){
    next();
  }else{
    let token=localStorage.getItem('mytoken');
    console.log(token);
    if(token===null || token==='' || token==='undefined'){
      next('/login');
    }else{
      next();
    }
  }
});
export default router
