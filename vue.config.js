module.exports = {
    devServer: {
      disableHostCheck: true,
      host:"localhost",
      port:8081,
      proxy:{
        "/api":{
          //target:"http://hhmy.ngrok2.xiaomiqiu.cn/",
          target:"http://localhost:5000/",
          changeOrigin:true,              //是否设置同源，输入是的
	        pathRewrite:{                   //路径重写
	          '/api':''                     //选择忽略拦截器里面的单词
	        }
        }
      }
    }
  }